const app = require('express');
let router = app.Router();
let Inmueble = require('../models/inmueble');
let Tipo = require('../models/tipo');
let fs = require('fs');

//3.1 Rutas para los inmuebles
router.get('/', (req, res) => {
    Inmueble.find().populate('tipo').then(resultado => {
        Tipo.find().then(result => {
            res.render('lista_inmuebles', {
                inmuebles: resultado,
                tipos: result
            });
        })
    }).catch(error => {
        console.log(error);
        res.render('lista_inmuebles', {
            inmuebles: []
        });
    });
});

//cambiado el :idTipo por el /tipo/:id por que se me solapaban las rutas
router.get('/tipo/:id', (req, res) => {
    Inmueble.find({
        tipo: req.params.id
    }).populate('tipo').then(resultado => {
        Tipo.find().then(result => {
            res.render('lista_inmuebles', {
                inmuebles: resultado,
                tipos: result
            });
        })
    }).catch(error => {
        console.log(error);
        res.render('lista_inmuebles', {
            inmuebles: []
        });
    });
});


router.post('/', (req, res) => {
    let img;
    let error;
    if (req.files.imagen) {
        let date = new Date();
        img = date.getDay() + date.getTime() + date.getUTCMilliseconds() + '.jpg';
        req.files.imagen.mv('public/uploads/' + img, error => {
            console.log('Error subiendo el archivo ', error);
        })
    } else
        img = 'piso.jpg';

    let inmueble = new Inmueble({
        descripcion: req.body.descripcion,
        tipo: req.body.tipo.trim(),
        habitaciones: req.body.habitaciones,
        superficie: req.body.superficie,
        precio: req.body.precio,
        imagen: img
    });

    inmueble.save().then(resultado => {
        Tipo.find().then(result => {
            res.render('ficha_inmueble', {
                inmueble: resultado,
                tipos: result
            });
        });
    });
});

router.delete('/:id', (req, res) => {
    Inmueble.findByIdAndRemove(req.params.id).then(resultado => {
        if (resultado.imagen != 'piso.jpg' && fs.existsSync('public/uploads/' + resultado.imagen)) {
            fs.unlink('public/uploads/' + resultado.imagen, () => null);
        }
        res.send({
            ok: true,
            resultado: resultado
        });
    }).catch(error => {
        res.send({
            ok: false,
            resultado: error
        });
    });
});

//filtrar segun el precio, superficie y habitaciones
router.get('/filtrar', (req, res) => {
    let precio = req.query.precio;
    let superficie = req.query.superficie;
    let habitaciones = req.query.habitaciones;
    console.log(req.query.precio, req.query.superficie, req.query.habitaciones);

    /*{ // information of express cassandra (find querys)
        $eq: '=',
        $ne: '!=', // applicable for IF conditions only
        $isnt: 'IS NOT', // applicable for materialized view filters only
        $gt: '>',
        $lt: '<',
        $gte: '>=',
        $lte: '<=',
        $in: 'IN',
        $like: 'LIKE', // applicable for sasi indexes only
        $token: 'token', // applicable for token queries only
        $contains: 'CONTAINS', // applicable for indexed collections only
        $contains_key: 'CONTAINS KEY', // applicable for indexed maps only
    }*/

    let filterObject = {};
    req.query.precio != 0 ? filterObject.precio = { $lte: req.query.precio } : "";
    req.query.superficie != 0 ? filterObject.superficie = { $gte: req.query.superficie } : "";
    req.query.habitaciones != 0 ? filterObject.numero_habitaciones = { $gte: req.query.habitaciones } : "";

    Inmueble.find(filterObject).populate('tipo').then(resultado => {
        Tipo.find().then(result => {
            res.render('lista_inmuebles', {
                inmuebles: resultado,
                tipos: result
            });
        })
    }).catch(error => {
        console.log(error);
    })
});

router.get('/:id', (req, res) => {
    Inmueble.findById(req.params.id).populate('tipo').then(resultado => {
        Tipo.find().then(result => {
            res.render('ficha_inmueble', {
                inmuebles: resultado,
                tipos: result
            });
        })
    }).catch(error => {
        console.log(error);
        res.render('ficha_inmueble', {
            inmuebles: [],
            tipos: []
        });
    });
});
module.exports = router;