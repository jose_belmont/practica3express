const express = require('express');
let router = express.Router();
let Tipo = require('../models/tipo');

router.get('/', (req, res) => {
    Tipo.find().then( resultado => {
        res.render('index', {tipos:resultado});
        
    })
});

router.get('/nuevo_inmueble', (req, res) => {
    Tipo.find().then(resultado => {
        console.log(resultado);
        res.render('nuevo_inmueble', { tipos: resultado });
    }).catch(error => {
        res.render('nuevo_inmueble', { tipos: [] })
    });
});

module.exports = router;