const express = require('express');
let Tipo = require('../models/tipo');
let router = express.Router();

//3.2 Rutas para los tipos
router.get('/', (req, res) => {
    Tipo.find().then(respuesta => {
        res.send({ tipos: respuesta }); //convierte la respuesta en json
    });
});

module.exports = router;